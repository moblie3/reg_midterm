import 'package:flutter/material.dart';

class BodyMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final urlImage = 'assets/images/Buu-logo.png';
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
        child: Column(children: <Widget>[
      Container(
          height: size.height * 0.2,
          child: Stack(children: <Widget>[
            Container(
              height: size.height * 0.2 - 27,
              decoration: BoxDecoration(
                  color: Colors.yellow,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(50),
                    bottomRight: Radius.circular(50),
                  ),
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(0, 10),
                      blurRadius: 50,
                      color: Colors.grey,
                    )
                  ]),
            ),
            Positioned(
              left: 150,
              top: 7,
              child: Image.asset(
                urlImage,
                width: 90,
                height: 90,
              ),
            ),
            Positioned(
                left: 100,
                top: 100,
                child: Text('มหาวิทยาลัยบูรพา',
                    style: TextStyle(
                        fontSize: 28.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey[600]))),
          ])),
      Container(
          child: Stack(
        children: <Widget>[
          Container(
            height: 600,
            decoration: BoxDecoration(
                color: Color.fromARGB(255, 252, 252, 252),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(36),
                  topRight: Radius.circular(36),
                ),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 10),
                    blurRadius: 50,
                    color: Color.fromARGB(255, 200, 198, 198),
                  )
                ]),
          ),
          Positioned(
              left: 40,
              top: 20,
              child: Container(
                  child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(320, 100),
                  textStyle: TextStyle(fontSize: 10),
                ),
                child: Text('เกี่ยวกับทะเบียนฯ'),
                onPressed: () {},
              ))),
              Positioned(
              left: 40,
              top: 130,
              child: Container(
                  child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(320, 100),
                  textStyle: TextStyle(fontSize: 10),
                ),
                child: Text('การทำบัตรนสิตกับธนาคารกรุงไทย'),
                onPressed: () {},
              ))),
              Positioned(
              left: 40,
              top: 240,
              child: Container(
                  child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(320, 100),
                  textStyle: TextStyle(fontSize: 10),
                ),
                child: Text('แบบประเมินความคดิเห็นของนักเรียนและ\nนิสิตต่อการให้บริการของสำนักงานอธิการบดี'),
                onPressed: () {},
              ))),
              Positioned(
              left: 40,
              top: 350,
              child: Container(
                  child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(320, 100),
                  textStyle: TextStyle(fontSize: 10),
                ),
                child: Text('ติดต่อเรา'),
                onPressed: () {},
              ))),
        ],
      ))
    ]));
  }
}
