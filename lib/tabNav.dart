import 'package:flutter/material.dart';
import 'menu.dart';
import 'profile.dart';
import 'login_page.dart';
import 'timetable.dart';

class  tabNav extends StatelessWidget{
  @override
   Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          Container(
                height: 50,
                decoration: BoxDecoration(
                  color: Colors.white,
                  
                ),
              ),
              ListTile(
            title: Text('REG BUU'),
          ),
            ListTile(
            leading: Icon(Icons.menu),
            title: Text('เมนูหลัก'),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => Menu(),));
            } ,
          ),
          ListTile(
            leading: Icon(Icons.person_outlined),
            title: Text('โปรไฟล์'),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => Profile(),));
            } ,
          ),
          ListTile(
            leading: Icon(Icons.assignment_add),
            title: Text('ลงทะเบียน'),
            onTap: () {
              AlertDialog alert = AlertDialog(title: Text('ไม่อยู่ในช่วงลงทะเบียน'),);
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return alert;
                },
              );
            } ,
          ),
          ListTile(
            leading: Icon(Icons.assignment),
            title: Text('ผลการลงทะเบียน'),
            onTap: () {
              AlertDialog alert = AlertDialog(title: Text('คุณยังไม่ได้ลงทะเบียน'),);
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return alert;
                },
              );
            } ,
          ),
          ListTile(
            leading: Icon(Icons.apps),
            title: Text('ตารางเรียน'),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => TimeTable(),));
            } ,
          ),
          ListTile(
            leading: Icon(Icons.school),
            title: Text('ผลการศึกษา'),
            onTap: () {} ,
          ),
          ListTile(
            leading: Icon(Icons.logout),
            title: Text('ออกจากระบบ'),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage(),));
            } ,
          ),
        ]
    )
    );
   }
}