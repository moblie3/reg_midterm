import 'package:flutter/material.dart';

class BodyTimetable extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final urlImage = 'assets/images/timetable.png';
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
    child: Column(
      children: <Widget>[
        Container(
          height: size.height * 0.2,
          child: Stack(
            children: <Widget>[
              Container(
                height: size.height * 0.2 - 27,
                decoration: BoxDecoration(
                  color: Colors.yellow,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(50),
                    bottomRight: Radius.circular(50),
                  ),
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(0,10),
                      blurRadius:  50,
                      color: Colors.grey,
                    )
                  ]
                ),
              ),
              Positioned(
              left: 37,
              top: 20,
              child: Container(
                  child: Text('ตารางเรียน'),
              )),
              
        ],
      )),
        Container(
          child: Stack(
        children: <Widget>[
          Container(
            height: 600,
            decoration: BoxDecoration(
                color: Color.fromARGB(255, 252, 252, 252),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(36),
                  topRight: Radius.circular(36),
                ),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 10),
                    blurRadius: 50,
                    color: Color.fromARGB(255, 200, 198, 198),
                  )
                ]),
          ),
          Positioned(
                  left: 25,
                  top: 15,
                  child: Image.asset(
                  urlImage,
                  width: 350,
                  height: 350,
                ),
                  ),
             
      ],
    )
    )
      ]));
    
  }
}

