import 'package:flutter/material.dart';
import 'bodyTimetable.dart';
import 'tabNav.dart';

class TimeTable extends StatelessWidget {
  const TimeTable({super.key});
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: tabNav(),
      appBar:buildAppBar(),
      body: BodyTimetable(),
    );
  }
    
  AppBar buildAppBar() {
    return AppBar(
      elevation: 0,
    );
  }

}