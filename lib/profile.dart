
import 'package:flutter/material.dart';
import 'bodyProfile.dart';
import 'tabNav.dart';

class Profile extends StatelessWidget {
  const Profile({super.key});
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: tabNav(),
      appBar:buildAppBar(),
      body: BodyProfile(),
    );
  }
    
  AppBar buildAppBar() {
    return AppBar(
      elevation: 0,
    );
  }

}
