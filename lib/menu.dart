import 'package:flutter/material.dart';
import 'bodyMenu.dart';
import 'tabNav.dart';

class Menu extends StatelessWidget {
  const Menu({super.key});
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: tabNav(),
      appBar:buildAppBar(),
      body: BodyMenu(),
    );
  }
    
  AppBar buildAppBar() {
    return AppBar(
      elevation: 0,
    );
  }

}