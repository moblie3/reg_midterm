import 'package:flutter/material.dart';

class BodyProfile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
    child: Column(
      children: <Widget>[
        Container(
          height: size.height * 0.2,
          child: Stack(
            children: <Widget>[
              Container(
                height: size.height * 0.2 - 27,
                decoration: BoxDecoration(
                  color: Colors.yellow,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(50),
                    bottomRight: Radius.circular(50),
                  ),
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(0,10),
                      blurRadius:  50,
                      color: Colors.grey,
                    )
                  ]
                ),
              ),
              Positioned(
                  left: 30,
                  top: 10,
                  child:Row(children:<Widget>[
                        CircleAvatar(
                          radius: 32,
                          backgroundImage: NetworkImage('https://scontent.fbkk5-3.fna.fbcdn.net/v/t1.6435-9/163304467_1565284193672069_1658584750240804527_n.jpg?_nc_cat=111&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeED64r7ilvwwpuGJ-w9VPf-J6NitKY09JEno2K0pjT0kUtjGfIbRxXCJmuTFFC3N6cmkdLs4Cb3IDg0uFEoz5AB&_nc_ohc=QHAqejPLC70AX_Xegi6&_nc_ht=scontent.fbkk5-3.fna&oh=00_AfCM1na4t1qeRobK0WXADghE_IvG3LdsmLSwVy1UfBcRBA&oe=63FFEE7B'),
                        ),
                        Column(
                          children: <Widget>[
                            Text('Kitdanai Sriraksa' ,style: TextStyle(fontSize: 20),) ,
                            Text('กำลังศึกษา'),
                          ],
                        )
                      ],)
              ),
              
        ],
      )),
        Container(
          child: Stack(
        children: <Widget>[
          Container(
            height: 600,
            decoration: BoxDecoration(
                color: Color.fromARGB(255, 252, 252, 252),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(36),
                  topRight: Radius.circular(36),
                ),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 10),
                    blurRadius: 50,
                    color: Color.fromARGB(255, 200, 198, 198),
                  )
                ]),
          ),
          Positioned(
              left: 37,
              top: 20,
              child: Container(
                  child: Text('63160063: นายกฤษณ์ดนัย ศรีรักษา'),
              )),
              Positioned(
              left: 37,
              top: 40,
              child: Container(
                  child: Text('คณะ: วิทยาการสารสนเทศ'),
              )),
              Positioned(
              left: 37,
              top: 60,
              child: Container(
                  child: Text('หลักสูตร: 2115020 วท.บ.(วิทนาการสารสนเทศ)\nปรับปรุง 59 - ป.ตรี 4 ปี ปกติ'),
              )),
              Positioned(
              left: 37,
              top: 100,
              child: Container(
                  child: Text('อ. ที่ปรึกษา: อาจารย์ภูสิต กุลเกษม,ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน'),
              )),
              Positioned(
              left: 300,
              top: 500,
              child: Container(
                  child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(50, 50),
                  textStyle: TextStyle(fontSize: 10),
                ),
                child: Text('แก้ไขข้อมููล'),
                onPressed: () {},
              ))),
      ],
    )
    )
      ]));
    
  }
}

